import { Post } from '../src/model/post';
import { PostsRepository } from '../src/repositories/posts.repository';
import { ModifyPostUseCase } from '../src/usecases/modify-post.usecase';

jest.mock('../src/repositories/posts.repository');

describe('Modify and update a post', () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });
  it('should update a post', async () => {
    const postToModify = new Post({
      id: 1,
      title: 'New updated title',
      content: 'New Updated body post',
    });

    PostsRepository.mockImplementation(() => {
      return {
        modifyPost: () => {
          return {
            id: postToModify.id,
            title: postToModify.title,
            body: postToModify.content,
            userId: 1,
          };
        },
      };
    });

    const POSTS = [
      {
        id: 1,
        title:
          'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
        content:
          'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto',
      },
      {
        id: 2,
        title: 'qui est esse',
        content:
          'est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla',
      },
    ];

    const modifiedPostList = await ModifyPostUseCase.execute(
      postToModify,
      POSTS
    );

    expect(modifiedPostList[0].title).toBe(postToModify.title);
    expect(modifiedPostList[0].content).toBe(postToModify.content);
  });
});

import { PostsRepository } from '../src/repositories/posts.repository';
import { CreatePostUseCase } from '../src/usecases/create-post.usecase';

jest.mock('../src/repositories/posts.repository');
describe('Add a new post', () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });
  it('should add a new post to the list', async () => {
    const post = {
      title: 'New Post',
      content: 'Content for the New Post',
    };

    PostsRepository.mockImplementation(() => {
      return {
        createPost: () => {
          return {
            id: 1,
            title: post.title,
            body: post.content,
            userId: 1,
          };
        },
      };
    });

    const POSTS = [
      {
        id: 1,
        title:
          'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
        content:
          'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto',
      },
      {
        id: 2,
        title: 'qui est esse',
        content:
          'est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla',
      },
    ];

    const listPosts = await CreatePostUseCase.execute(post, POSTS);

    expect(listPosts.length).toBe(POSTS.length + 1);
    expect(listPosts[0].title).toBe(post.title);
    expect(listPosts[0].content).toBe(post.content);
  });
});

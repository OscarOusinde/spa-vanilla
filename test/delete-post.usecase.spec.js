import { PostsRepository } from '../src/repositories/posts.repository';
import { DeletePostUseCase } from '../src/usecases/delete-post.usecase';

jest.mock('../src/repositories/posts.repository');

describe('Delete a post by id', () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });
  it('should delete a post', async () => {
    const postToDelete = 1;

    const POSTS = [
      {
        id: 1,
        title:
          'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
        content:
          'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto',
      },
      {
        id: 2,
        title: 'qui est esse',
        content:
          'est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla',
      },
    ];

    PostsRepository.mockImplementation(() => {
      return {
        deletePost: () => {},
      };
    });

    const listPosts = await DeletePostUseCase.execute(postToDelete, POSTS);

    expect(listPosts.length).toBe(1);
    expect(listPosts[0].id).toBe(2);
    expect(listPosts[0].title).not.toBe(postToDelete.title);
    expect(listPosts[0].content).not.toBe(postToDelete.content);
  });
});

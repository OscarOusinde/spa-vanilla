describe('template spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080');
    cy.get('.postsList__addButton').click();
    cy.get('.postDetail__form__inputsFieldset__postTitleInput').type(
      'New post title'
    );
    cy.get('.postDetail__form__inputsFieldset__postBodyTextarea').type(
      'New post body'
    );
    cy.get('.postDetail__form__buttonsFieldset__addButton').click();
    cy.get(
      ':nth-child(1) > .postsList__ulPostsList__liPostsList__postUi > .postsList__ulPostsList__liPostsList__postUi__pPostUi'
    ).contains('New post title');
    cy.get('.postsList__ulPostsList > :nth-child(1)').click();
    cy.get('.postDetail__form__inputsFieldset__postTitleInput').should(
      'have.value',
      'New post title'
    );
    cy.get('.postDetail__form__inputsFieldset__postBodyTextarea').should(
      'have.value',
      'New post body'
    );
  });
});

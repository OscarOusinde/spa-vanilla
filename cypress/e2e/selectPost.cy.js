describe('template spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080');
    cy.get('.postsList__ulPostsList > :nth-child(1)').click();
    cy.get('.postDetail__form__buttonsFieldset')
      .should('have.descendants', 'button')
      .find('button')
      .should('have.length', 3);
    cy.get('.postDetail__form__inputsFieldset__postTitleInput').should(
      'not.have.value',
      ''
    );
    cy.get('.postDetail__form__inputsFieldset__postBodyTextarea').should(
      'not.have.value',
      ''
    );
  });
});

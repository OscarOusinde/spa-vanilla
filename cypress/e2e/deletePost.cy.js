describe('template spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080');
    cy.get('.postsList__ulPostsList > :nth-child(1)').click();
    cy.get('.postDetail__form__buttonsFieldset__deleteButton').click();
    cy.get(
      ':nth-child(1) > .postsList__ulPostsList__liPostsList__postUi > .postsList__ulPostsList__liPostsList__postUi__pPostUi'
    ).should(
      'not.have.value',
      'sunt aut facere repellat provident occaecati excepturi optio reprehenderit'
    );
  });
});

describe('template spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080');
    cy.get('.postsList__ulPostsList > :nth-child(1)').click();
    cy.get('.postDetail__form__inputsFieldset__postTitleInput')
      .clear()
      .type('This is an edited title post');
    cy.get('.postDetail__form__inputsFieldset__postBodyTextarea')
      .clear()
      .type('This is an edited body post');
    cy.get('.postDetail__form__buttonsFieldset__updateButton').click();
    cy.get(
      ':nth-child(1) > .postsList__ulPostsList__liPostsList__postUi > .postsList__ulPostsList__liPostsList__postUi__pPostUi'
    ).should(
      'not.have.value',
      'sunt aut facere repellat provident occaecati excepturi optio reprehenderit'
    );
  });
});

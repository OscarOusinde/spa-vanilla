import { LitElement, html } from 'lit';

export class PostUI extends LitElement {
  static get properties() {
    return {
      post: { type: Object },
    };
  }

  render() {
    return html`<p class="postsList__ulPostsList__liPostsList__postUi__pPostUi">
      ${this.post?.title}
    </p>`;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('post-ui', PostUI);

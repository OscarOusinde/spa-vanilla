import { LitElement, html } from 'lit';

export class PostDetail extends LitElement {
  static get properties() {
    return {
      listPosts: { type: Array },
      completePost: { type: Array },
      addMode: { type: Boolean },

      titlePostInput: { type: String },
      bodyPostInput: { type: String },
      newPost: { type: Object },
    };
  }

  async connectedCallback() {
    super.connectedCallback();

    this.addMode = true;

    //Listening events
    const postListsComponent = document.querySelector('posts-list');
    postListsComponent.addEventListener('postEdition', (e) => {
      this.completePost = e.detail.completePost;
      this.listPosts = e.detail.postsList;
      this.addMode = false;
    });

    postListsComponent.addEventListener('addMode', (e) => {
      this.addMode = e.detail.addMode;
    });
  }

  render() {
    return html`<h2 class="postDetail__title">Post Detail</h2>
      <form class="postDetail__form">
        <fieldset class="postDetail__form__inputsFieldset">
          <label
            for="postTitle"
            class="postDetail__form__inputsFieldset__postTitleLabel"
            >Title</label
          >
          <input
            name="postTitle"
            placeholder="${this.addMode
              ? 'Write a title for a new post'
              : 'Select another post or press Cancel to add a new post'}"
            type="text"
            .value="${this.addMode ? '' : this.completePost[0]?.title}"
            class="postDetail__form__inputsFieldset__postTitleInput"
          />
          <label
            for="postBody"
            class="postDetail__form__inputsFieldset__postBodyLabel"
            >Body</label
          >
          <textarea
            name="postBody"
            placeholder="${this.addMode
              ? 'Write a message body for a new post'
              : 'Select another post or press Cancel to add a new post'}"
            wrap="soft"
            type="text"
            .value="${this.addMode ? '' : this.completePost[0]?.content}"
            class="postDetail__form__inputsFieldset__postBodyTextarea"
          ></textarea>
        </fieldset>
        <fieldset class="postDetail__form__buttonsFieldset">
          ${this.addMode
            ? html`<button
                  @click="${this.addPost}"
                  class="postDetail__form__buttonsFieldset__addButton"
                >
                  Add
                </button>
                <button
                  @click="${this.cancelButton}"
                  class="postDetail__form__buttonsFieldset__cancelButton"
                >
                  Cancel
                </button>`
            : html`
                <button
                  @click="${this.updatePost}"
                  class="postDetail__form__buttonsFieldset__updateButton"
                >
                  Update
                </button>
                <button
                  @click="${this.deletePost}"
                  class="postDetail__form__buttonsFieldset__deleteButton"
                >
                  Delete
                </button>
                <button
                  @click="${this.cancelButton}"
                  class="postDetail__form__buttonsFieldset__cancelButton"
                >
                  Cancel
                </button>
              `}
        </fieldset>
      </form> `;
  }

  updatePost(e) {
    e.preventDefault();
    this.titlePostInput = document.getElementsByName('postTitle')[0].value;
    this.bodyPostInput = document.getElementsByName('postBody')[0].value;

    this.completePost[0].title = this.titlePostInput;
    this.completePost[0].content = this.bodyPostInput;
    this.completePost[0].userId = 1;

    const postToModify = new CustomEvent('updatePost', {
      bubbles: true,
      composed: true,
      detail: {
        postToUpdate: this.completePost[0],
      },
    });
    this.dispatchEvent(postToModify);
  }

  deletePost(e) {
    e.preventDefault();

    const idPostToDelete = new CustomEvent('deletePost', {
      bubbles: true,
      composed: true,
      detail: {
        idPostToDelete: this.completePost[0].id,
      },
    });
    this.dispatchEvent(idPostToDelete);

    document.querySelector('form').reset();
  }

  addPost(e) {
    e.preventDefault();
    this.titlePostInput = document.getElementsByName('postTitle')[0].value;
    this.bodyPostInput = document.getElementsByName('postBody')[0].value;

    this.newPost = {
      title: this.titlePostInput,
      content: this.bodyPostInput,
    };

    const newPost = new CustomEvent('newPost', {
      bubbles: true,
      composed: true,
      detail: {
        newPost: this.newPost,
      },
    });
    this.dispatchEvent(newPost);

    document.querySelector('form').reset();
  }

  cancelButton(e) {
    e.preventDefault();
    document.querySelector('form').reset();
    this.addMode = true;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('post-detail', PostDetail);

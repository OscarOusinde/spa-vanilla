import { LitElement, html } from 'lit';
import { AllPostsUseCase } from '../usecases/all-posts.usecase';
import { CreatePostUseCase } from '../usecases/create-post.usecase';
import { DeletePostUseCase } from '../usecases/delete-post.usecase';
import { ModifyPostUseCase } from '../usecases/modify-post.usecase';
import '../ui/post.ui';

export class PostsLists extends LitElement {
  static get properties() {
    return {
      listPosts: { type: Array },
      completePost: { type: Array },
      newPost: { type: Object },
      idPostToDelete: { type: Number },
      postToUpdate: { type: Object },
      error: {},
    };
  }

  async connectedCallback() {
    super.connectedCallback();

    //AllPostUseCase
    this.listPosts = await AllPostsUseCase.execute();

    //CreatePostUseCase
    const postDetailComponent = document.querySelector('post-detail');
    postDetailComponent.addEventListener('newPost', async (e) => {
      this.newPost = e.detail.newPost;

      this.listPosts = await CreatePostUseCase.execute(
        this.newPost,
        this.listPosts
      );
    });

    //DeletePostUseCase
    postDetailComponent.addEventListener('deletePost', async (e) => {
      this.idPostToDelete = e.detail.idPostToDelete;

      this.listPosts = await DeletePostUseCase.execute(
        this.idPostToDelete,
        this.listPosts
      );
    });

    //ModifyPostUseCase
    postDetailComponent.addEventListener('updatePost', async (e) => {
      this.postToUpdate = e.detail.postToUpdate;

      this.listPosts = await ModifyPostUseCase.execute(
        this.postToUpdate,
        this.listPosts
      );
    });
  }

  render() {
    return html` <button @click="${this.addMode}" class="postsList__addButton">
        Add
      </button>
      <h2 class="postsList__title">Posts List</h2>
      <ul class="postsList__ulPostsList">
        ${this.listPosts?.map(
          (post) =>
            html`<li class="postsList__ulPostsList__liPostsList">
              <post-ui
                @click="${this.editMode}"
                .post="${post}"
                class="postsList__ulPostsList__liPostsList__postUi"
              ></post-ui>
            </li>`
        )}
      </ul>`;
  }

  addMode(e) {
    e.preventDefault();
    const addMode = new CustomEvent('addMode', {
      bubbles: true,
      composed: true,
      detail: {
        addMode: true,
      },
    });
    this.dispatchEvent(addMode);
  }

  editMode(e) {
    e.preventDefault();

    this.completePost = this.listPosts.filter(
      (post) => post.title === e.target.innerText
    );

    const post = new CustomEvent('postEdition', {
      bubbles: true,
      composed: true,
      detail: {
        completePost: this.completePost,
        postsList: this.listPosts,
      },
    });
    this.dispatchEvent(post);
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('posts-list', PostsLists);

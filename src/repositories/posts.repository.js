import axios from 'axios';

export class PostsRepository {
  async getAllPosts() {
    return await (
      await axios.get('https://jsonplaceholder.typicode.com/posts')
    ).data;
  }

  async createPost(post) {
    const dataPost = {
      title: post.title,
      body: post.content,
      userId: 1,
    };
    return await (
      await axios.post('https://jsonplaceholder.typicode.com/posts', dataPost)
    ).data;
  }

  async deletePost(postToDelete) {
    return await axios.delete(
      `https://jsonplaceholder.typicode.com/posts/${postToDelete}`
    );
  }

  async modifyPost(postToModify) {
    const post = {
      id: postToModify.id,
      title: postToModify.title,
      body: postToModify.content,
      userId: 1,
    };
    return await (
      await axios.put(
        `https://jsonplaceholder.typicode.com/posts/${post.id}`,
        post
      )
    ).data;
  }
}

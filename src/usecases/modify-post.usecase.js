import { PostsRepository } from '../repositories/posts.repository';

export class ModifyPostUseCase {
  static async execute(postToModify, postsList) {
    try {
      const repository = new PostsRepository();
      const postModified = await repository.modifyPost(postToModify);

      const postUpdated = {
        id: postModified.id,
        title: postModified.title,
        content: postModified.body,
      };

      return postsList.map((post) =>
        post.id === postUpdated.id ? postUpdated : post
      );
    } catch (error) {
      alert(
        'Something went wrong. This API does not support modification of the created posts. Try another alternative action like deletion or update the native API posts'
      );
      return postsList;
    }
  }
}

import { Post } from '../model/post';
import { PostsRepository } from '../repositories/posts.repository';

export class CreatePostUseCase {
  static async execute(post, listPosts) {
    const repository = new PostsRepository();
    const newPost = await repository.createPost(post);

    const addedPost = new Post({
      id: newPost.id,
      title: newPost.title,
      content: newPost.body,
    });

    const newPostsList = [addedPost, ...listPosts];

    return newPostsList;
  }
}

export class oddPostsUseCase {
  static execute(listPosts) {
    const oddPosts = listPosts.filter((odd) => odd.id % 2 !== 0);
    return oddPosts;
  }
}

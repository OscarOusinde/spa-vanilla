import { PostsRepository } from '../repositories/posts.repository';

export class DeletePostUseCase {
  static async execute(postToDelete, postsList) {
    const repository = new PostsRepository();
    await repository.deletePost(postToDelete);

    return postsList.filter((post) => post.id !== postToDelete);
  }
}
